#!/bin/bash
for i in *.pdf; do
   pdftk "$i"  dump_data |sed -e 's/\(InfoValue:\)\s.*/\1\ /g' | pdftk "$i" update_info - output "a_$i"
   mv "a_$i" "$i"
done
