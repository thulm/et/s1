# Was ist Anki?
Alles was du wissen musst steht im [README_ANKI.md](https://gitlab.com/thulm/et/s1/-/tree/main/README_ANKI) vom 1. Semester.

# Kommentar zu MATH1_AK_HundWelsch
Viele Karten beziehen sich auf Algorithmen, welche oft je nach Prof unterschiedlich durchgeführt werden. Dieses Kartendeck ist passend zur Vorlesung von Max