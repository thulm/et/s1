# Anki
Wie immer gibt es praktische Tutorials auf YouTube, hier jedoch das wichtigste zusammengefasst.

## Was ist Anki?
Anki ist eine Lernsoftware, die auf Spaced Repetition basiert, um das Langzeitgedächtnis zu verbessern. 
Benutzer können eigene Karteikarten-Sets erstellen und diese regelmäßig wiederholen, um den Lernerfolg zu steigern. 
Es ist ein effizientes Werkzeug zum Vokabellernen, Studieren und Merken von Informationen in verschiedenen Bereichen.

## Anki installieren
### Windows/macOS/Linux
Um Anki auf deinem PC zu installieren, folge  diesen Schritten:
1. Öffne die offizielle Anki-Website: https://apps.ankiweb.net/
2. Klicken auf den Link "Anki herunterladen", um zur Download-Seite zu gelangen.
3. Wählen die passende Version für dein Betriebssystem (Windows, macOS oder Linux) aus.
4. Starten die heruntergeladene Installationsdatei und befolgen Sie die Anweisungen des Installationsassistenten.
5. Sobald die Installation abgeschlossen ist, öffne Anki über das Startmenü (unter Windows) oder das Anwendungsverzeichnis (unter macOS).
Nach dem Start der Anki-Software kannst du sofort mit dem Erstellen von Karteikarten beginnen oder vorhandene Decks nutzen.
Sehr zu empfelen ist sich ein Konto auf AnkiWeb zu erstellen, um deine Karteikarten zu synchronisieren und von verschiedenen Geräten aus darauf zugreifen zu können.

### IOS
Um Anki unter IOS zu nutzen, gehe wie folgt vor:
1. Öffne den App Store auf deinem Gerät.
2. Suche nach "[AnkiMobile Flashcards](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387)" und lade die offizielle AnkiMobile App herunter. (Es gibt auch kostenlose Varianten, welche aber meistens nicht so gut funktionieren. Gerne eure Erfahrungen teilen, mir war es jedoch mehr als Wert die offizielle App zu kaufen @ThorbenCarl)
3. Starte die AnkiMobile App nach der Installation. Falls du bereits ein Anki-Konto hast und deine Karteikarten mit AnkiWeb synchronisiert hast, melde dich in der App an, um deine Karteikarten zu übertragen.
4. Nach der Anmeldung werden deine Karteikarten synchronisiert und stehen dir auf dem Gerät zur Verfügung.
5. Jetzt kannst du deine Karteikarten durchgehen, neue Karten erstellen oder vorhandene Decks bearbeiten.
Die App ermöglicht es dir, auch unterwegs effizient zu lernen und deine Karteikarten auf dem Gerät zu speichern, selbst wenn du keine Internetverbindung hast.

### Android
Die "offizielle" (entwickelt von der Anki Community) Android App heißt: [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki). Funktinoiert sehr gut auch mit vielen Erweiterungen, ansonsten die [Dokumentation](https://docs.ankidroid.org/) konsultieren.
Die Kompetenz zur Installation und Nutzung einer Android App unterstelle ich Dir als Leser ;D und verzichte deswegen auf eine weitere Beschreibung.  

## Karten verwaltung
- Wichtig ist zu wissen das die Synchronisation nur mit einem Anki-Account funktioniert und **nicht** automatisch erfolgt sondern jedesmal manuell durchgeführt werden muss. 

### Wie importiere Karten/Decks?
Um ein bestehendes Anki-Deck zu importieren, befolge diese Schritte:
1. Stelle sicher, dass du das Anki-Deck in einem unterstützten Dateiformat hast. Anki unterstützt den Import von Decks in den Formaten ".apkg" (Anki-Paketdatei) und ".anki2" (SQLite-Datenbank).
2. Öffne die Anki-Software auf deinem PC (falls du das Deck auf dem iPad/iPhone importieren möchtest, importiere es zuerst auf dem PC und synchronisiere dann die Daten mit der AnkiMobile-App auf dem iPad/iPhone).
3. Gehe zu "Datei" in der oberen Menüleiste und wähle "Datei importieren".
4. Wähle die entsprechende Datei des Decks aus, das du importieren möchtest.
5. Anki wird dich nun nach den Einstellungen für den Import fragen. In den meisten Fällen kannst du die Standardeinstellungen belassen. Du kannst jedoch auch Optionen wie "Duplikate hinzufügen" oder "Tags importieren" auswählen, je nachdem, wie du das Deck importieren möchtest.
6. Klicke auf "Importieren", um den Vorgang zu starten.
7. Nach dem Import werden die Karten und Decks in deiner Anki-Bibliothek sichtbar sein. Du kannst auf die Decks zugreifen und mit dem Lernen beginnen.

Falls du das Deck auf dem iPad/iPhone verwenden möchtest, synchronisiere deine Anki-Daten, indem du dich in der AnkiMobile App auf dem iPad/iPhone mit deinem Anki-Konto anmeldest. 
Dadurch werden alle Änderungen und Importe, die du auf dem PC gemacht hast, auch auf dein iPad/iPhone übertragen.

### Wie erstelle ich Karten?
Um eigene Karten und Decks in Anki zu erstellen, folge diesen Schritten:
1. Öffne die Anki-Software auf deinem PC oder iPad/iPhone.
2. Klicke auf `Stapel erstellen` unten im Anki Fenster, um ein neues Deck zu erstellen. Gib dem Deck einen Namen und bestätige die Erstellung. (macOS)
3. Wähle das erstellte Deck aus, um es auszuwählen, oder klicke auf `Stapel erstellen`, um weitere Decks zu erstellen, die du benötigst.
4. Um eine neue Karte zu erstellen, gehe zu dem gewünschten Deck und klicke auf `Hinzufügen` (macOS) oder auf `Neu` (auf dem iPad/iPhone).
5. Es öffnet sich ein Bearbeitungsfenster, in dem du deine Frage und Antwort für die Karte eingeben kannst. Du kannst auch zusätzliche Informationen wie Tags oder Audio hinzufügen, falls gewünscht.
6. Klicke auf `Hinzufügen`, um die Karte zu erstellen und zum Bearbeitungsfenster zurückzukehren.
7. Du kannst weitere Karten auf die gleiche Weise hinzufügen, indem du auf `Hinzufügen` (macOS) oder auf `Neu` (auf dem iPad/iPhone).
8. Um Karten in einem vorhandenen Deck zu organisieren, kannst du Tags verwenden, um ähnliche Karten zu markieren. Dadurch kannst du später gezielt nach bestimmten Karten suchen und sie schneller finden.
9. Wenn du fertig bist, klicke auf "Schließen" oder "Beenden", um das Bearbeitungsfenster zu verlassen und zur Hauptansicht zurückzukehren.

Deine erstellten Karten und Decks werden jetzt in deiner Anki-Bibliothek angezeigt. 
Du kannst sie durchgehen und mit dem Lernen beginnen. 
Vergiss nicht, die Anki-Software regelmäßig zu synchronisieren, wenn du auf mehreren Geräten arbeitest, damit deine Karten auf allen Geräten verfügbar und auf dem neuesten Stand sind.

#### Plugins
In Anki sind Plugins zusätzliche Erweiterungen, die von der Benutzergemeinschaft entwickelt wurden, um die Funktionalität der Anki-Software zu erweitern und das Lernerlebnis zu verbessern. Mit Plugins kannst du neue Funktionen hinzufügen, das Erscheinungsbild der Karten anpassen und vieles mehr.

##### Image Occlusion Enhanced
Das Plugin "Image Occlusion Enhanced" ist ein leistungsfähiges Add-on für Anki, das es Benutzern ermöglicht, effektiv und interaktiv Lernkarten mit ausgeblendeten Bildern zu erstellen. 
Diese Art von Karten eignet sich besonders gut für das Lernen von anatomischen Strukturen, geografischen Karten, Diagrammen oder anderen visuellen Konzepten, bei denen du bestimmte Teile des Bildes identifizieren oder benennen musst.

Zur Installation des Plugins "Image Occlusion Enhanced" in Anki gehst du wie folgt vor: (ChatGPT, nicht überprüft!)
1. Starte die Anki-Software und öffne dein Anki-Konto, falls du bereits eines hast. Andernfalls kannst du ein neues Konto erstellen.
2. Gehe zum "Add-ons" (Erweiterungen) Menü in Anki, indem du auf "Werkzeuge" in der oberen Menüleiste klickst und dann "Add-ons" auswählst.
3. Klicke auf "Code abrufen" (Get Add-ons) im Add-ons-Fenster. Dadurch öffnet sich ein neues Fenster, in dem du nach verfügbaren Add-ons suchen kannst
4. In das Suchfeld des Add-ons-Fensters gibst du "Image Occlusion Enhanced" ein und klickst auf den Suchen-Button.
5. Das "Image Occlusion Enhanced" Add-on sollte in der Suchergebnisliste erscheinen. Klicke auf den "Installieren"-Button neben dem Add-on, um es zu installieren.
6. Anki wird dich warnen, dass das Add-on möglicherweise nicht sicher ist. Klicke auf "Ja", um die Installation zu bestätigen.
7. Nach der Installation wird Anki dich auffordern, die Software neu zu starten, um die Änderungen zu übernehmen. Klicke auf "Ja", um Anki neu zu starten.
8. Sobald Anki neu gestartet ist, ist das "Image Occlusion Enhanced" Add-on aktiviert und einsatzbereit.

Das Add-on ermöglicht dir das Erstellen von ausgeblendeten Bildern (Image Occlusion), die besonders nützlich sind, um Lernkarten für anatomische Strukturen oder ähnliche Konzepte zu erstellen. 
Um diese Funktion zu nutzen, öffnest du ein Deck, klickst auf "Karte hinzufügen" und wählst dann den "Image Occlusion"-Kartentyp aus.

Vergiss nicht, deine Arbeit regelmäßig zu speichern und dein Anki-Konto zu synchronisieren, um sicherzustellen, dass deine Daten gesichert sind und auf allen Geräten verfügbar sind, auf denen du Anki verwendest.

### Wie strukturiere ich Karten?
Wenn du ein neues Deck erstellst oder umbenennst kannst du Ordnerstrukturen mit `::` erstellen. <br>
Bsp.: `Ordner::erster_Unterordner::zweiter_Unterordner`

### Wie exportiere ich Karten?
Um ein Deck in Anki zu exportieren, folge diesen Schritten:

1. Klicke in der linken Seitenleiste auf das Deck, das du exportieren möchtest, um es auszuwählen. Falls das Deck Unterdecks enthält und du das gesamte Deck mit allen Unterdecks exportieren möchtest, wähle einfach das Hauptdeck aus.
2. Wähle beim Zahnrad neben dem Deck "Exportieren" oder "Export" aus. (macOS)
4. Es öffnet sich ein Dialogfenster, in dem du die Exportoptionen auswählen kannst. Hier kannst du verschiedene Einstellungen vornehmen, u.a.:
   - Exportformat: Wähle das gewünschte Format für den Export. Das gängigsten Formate ist "Anki-Paket (.apkg)".
   - Inklusive Medien: Wenn du Bilder, Audio oder Videos in deinen Karten hast, kannst du diese Option aktivieren, um sie zusammen mit den Karten zu exportieren.
5. Wähle die gewünschten Optionen und den Speicherort für den Export aus.
6. Klicke auf "Exportieren" oder "Export", um den Vorgang zu starten.

Anki wird nun das ausgewählte Deck (oder die ausgewählten Decks) in dem von dir gewählten Format exportieren. 
Du kannst die exportierte Datei dann speichern und für den späteren Import in Anki oder zum Teilen mit anderen verwenden.

Erinnerung: Denke daran wenn du Anki-Decks für andere bereitstellst, dass keine Urheberrechte verletzt!