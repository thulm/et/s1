# 1. Semester Betreuer: @?
## :book: Inhalt
1. [DIGT1_Digitaltechnik](https://gitlab.com/thulm/et/s1/-/tree/main/DIGT1_Digitaltechnik)
2. [ETGR1_Elektrotechnik](https://gitlab.com/thulm/et/s1/-/tree/main/ETGR1_Elektrotechnik)
3. [MATH1_Mathematik](https://gitlab.com/thulm/et/s1/-/tree/main/MATH1_Mathematik)
4. [PHYS1_Physik](https://gitlab.com/thulm/et/s1/-/tree/main/PHYS1_Physik)
5. [PROGC_Programmieren_in_C](https://gitlab.com/thulm/et/s1/-/tree/main/PROGC_Programmieren_in_C)

# :speech_balloon: WhatsApp Gruppen
[THU ET1/2d Start: 24SS]() **Link fehlt!** <br>
[THU ET2/3d Start: 23WS]() **Link fehlt!** <br>
[THU ET3/6d Start: 23SS]() **Link fehlt!** <br>
[THU ET4/7d Start: 22WS](https://chat.whatsapp.com/G6dX9VnRI3hDQ1HXlpRpFx) <br>
[THU ET5    Start: 22SS](https://chat.whatsapp.com/GDVDNlR8O1xLIwYq4idENv) <br>
[THU ET6/8d Start: 21WS](https://chat.whatsapp.com/EPA0is5g1fgAoC5u4Dr2zM) <br>
[THU ET7/9d Start: 21SS](https://chat.whatsapp.com/FvOVZEMvMEOKpq3C9J36Sr) <br>
<br>
[THU ET Abschluss 23WS](https://chat.whatsapp.com/Dd2M6LO4qoP7qmLet0O3mA) <br>
[THU ET Abschluss 23SS](https://chat.whatsapp.com/JXuiuIbcmu35U6Qq6s8G53) <br>
[Best of Ulm ET/DM](https://chat.whatsapp.com/GpukpodzFw9DVG76j4kldC) <br>

Die Gruppen wachsen über die Semester mit. Am ende des Semester Links bitte entsprechend weiterschieben und Gruppen umbenennen. Duale Studenten wechseln dann in die entsprechenden Gruppen.

# :speaking_head: Mitwirken? Unbedingt!
Die Repos sind von Studierenden für Studierende, jeder profitiert vom anderen! Hilf mit und gib deinen Senf dazu: Ergänze die Materialien, korrigiere Fehler oder lad' neue Infos hoch: Auch wenn vieles gleich bleibt, auch kleine Details können einen großen Unterschied ausmachen!
Damit die Zusammenarbeit mit so vielen Leuten gut funktioniert und um dir den Einstieg zu erleichtern, findest du in den folgenden Abschnitten einige hilfreiche Informationen.
Und übrigens, lass dich von Git nicht abschrecken! Die ersten Schritte sind zwar etwas unübersichtlich - aber die Kenntnisse, die man im Umgang mit diesem Versionierungs-System erwirbt, werden sich im weiteren Verlauf des Studiums als äußerst nützlich erweisen.

## :warning: Fehler gefunden? Verbesserungen? Ergänzungen?
Du hast einen Fehler gefunden, Verbesserungsvorschläge oder ergänzende Dokumente/Erklärungen? Dann wird es Zeit zum Handeln:
### :card_index: Issues erstellen
Im Issue (Ticket) kannst du auf einen Fehler hinweisen oder melden, wenn etwas fehlt. Auch z.B., wenn du gerade keine Zeit hast es zu verbessern.
### :writing_hand: Änderungen erstellen
Aus einem Issue erstellst du einen neuen Branch und pushest auf diesen deine Änderungen. Anschließend stellst du einen Merge Request von deinem neuen Branch in den main Branch, um deine Änderungen allen zur Verfügung zu stellen. Nach dem deine Änderungen Approved (mind. Developer) wurden, werden sie gemerged. Dies übernimmt ein Admin oder Semesterbetreuer. Damit ein Admin von deinem Merge Request etwas mitbekommt, trage ihn dann bitte als Reviewer ein. <br> 
Wenn du das Arbeiten mit git noch nicht kennst, gibt es eine Vielzahl an Tutorials im Internet. Bei Unklarheiten einfach an @ThorbenCarl wenden.
### :file_folder: Welche Dokumente soll ich pushen?
Alles ist willkommen was andern weiter hilft. Achte darauf, dass möglichst immer ein PDF deines Dokumentes zur Verfügung steht. Nach Möglichkeit schreibe die Lösung nicht gleich unter die Aufgabe, jedoch z. B. an das Ende des Dokuments, sodass man alles zusammen in einem Dokument hat. 
Markdown und LaTeX eigenen sich besonders gut. (Bei LaTeX bitte nur die Dateien, die zum Bauen des Dokumentes benötigt werden, meisten nur `.tex` Dateien). MS Word etc. sind in den meisten Fällen eher unpraktisch in git. <br>
Hast du in deinen `.md` oder `.tex` Dateien Bilder eingebunden? Dann erstelle einen Ordner mit dem gleichen Namen der Datei und lege dort deine Importe ab. Bsp.:
```
PHYS1_ZF_Nachbauer.md
PHYS1_ZF_Nachbauer.pdf
PHYS1_ZF_Nachbauer
```
### :x: Was darf nicht gepushed werden?
Alle Dokumente, an den du keine Rechte hast. Solange ein Prof. nicht ausdrücklich zur Veröffentlichung zustimmt, ist die Veröffentlichung strengstens verboten, egal ob alte Klausuren, Skripte o. ä. <br>
Es ist auch nicht das Ziel Dokumente zu duplizieren, heißt alles, was von der THU veröffentlicht wird, egal ob Laufwerke, Moodle, … hat hier nichts verloren. Übungen oder Klausuren natürlich gerne verlinken.

#### Metadaten 
Möchtest du deine Metadaten aus deinen PDF entfernen? Nutze das von @olagino bereitgestellte Skript: [remMeta.sh](https://gitlab.com/thulm/et/s1/-/blob/main/remMeta.sh)

## :card_box: Repo Struktur und Dateinamen
```
└──LSFKurzbezeichnung(NR)_Name (Bsp.: PHYS1_Physik)
   ├──Skripte
   │  ├──LSFKurzbezeichnung(NR)_Typ_Nachname 
   │  └──(Bsp.: PHYS1_SK_Nachbauer.pdf)
   ├──Praesentationen
   ├──Vorlesungsvideos
   ├──Zusammenfassungen
   │  ├──LSFKurzbezeichnung(NR)_Typ_Nachname 
   │  └──PHYS1_ZF_Nachbauer.pdf (Bsp.)
   ├──Labore
   │  ├──LSFKurzbezeichnung(NR)_Typ_NR_Nachname 
   │  └──PHYS1_LA_1_Carl.pdf (Bsp.)
   ├──Karteikarten
   │  ├──LSFKurzbezeichnung(NR)_Typ_Nachname 
   │  └──PHYS1_AK_Carl (Bsp.)
   ├──Uebungen
   ├──Klausuren
   │  ├──LSFKurzbezeichnung(NR)_Typ_JahrSemester_Nachname 
   │  └──PHYS1_KL_20w_Carl (Bsp.)
   └──Tutorium
      ├──LSFKurzbezeichnung(NR)_Typ_Datum_Nachname 
      └──PHYS1_TU_230322_Nachbauer (Bsp.)

Abkürzungen:
- AK = Anki
- AU = Aufgabe
- KL = Klausur 
- LA = Labor
- LS = Lösung
- MI = Mindmap
- SK = Skript
- TU = Tutorium
- UE = Übung
- VI = Video
- ZF = Zusammenfassung
```

<!-- ## :bookmark: Erfahrungsberichte 
In jedem Modul des Semesters oder Schwerpunktens findest du Erfahrungsberichte zu den jeweiligen Modulen. Teile auch gerne deine Erfahrungen und Tipps.

### Regeln
Ziel ist es nicht irgendwelche Profs. zu Bewerten oder gar bloßzustellen. Ziel ist es zukünftigen Studierenden Tipps für die Vorlesung/Klausur zu geben. Profs. werden nicht namentlich genannt, schreibt lieber worum es in der Vorlesung geht und was man beim Lernen beachten sollte. Gerade bei Wahlfächern soll dies als eine mögliche Erweiterung zur Entscheidungsfindung dienen.
Unpassende Beiträge sind zu entfernen!

#### Vorlage
```
### Erfahrungen/Tipps @username SXYY
#### Vorlesung
- Freitext
#### Klausur
- Freitext
``` -->

# :eyes: Admins
- Owner: Tim Nachbauer (@darulin007, nachti01@thu.de, Alumni 23WS) 
- Owner: Thorben Carl (@ThorbenCarl, thorben.carl@gmail.com, ET7/9d)
- Developer: Lorenz Hund (@lorenzhund.lh, lorenzhund.lh@gmail.com, ET5)
- Developer: Sebastian Esch (@sespaetzlean, eschse01@thu.de, ET7)

## Semesterbetreuer
1. ? 
2. ?
3. Lukas Meixner (@meixnerluk, lukas_meixner@gmx.de, ET3)
4. Leon Welsch (@derwelsch, leon.welsch@outlook.com, ET4/7d)

## Schwerpunktbetreuer
- **Automatisierung**: Lorenz Hund (@lorenzhund.lh, lorenzhund.lh@gmail.com, ET5)
- **Fahrzeugsysteme**: Daniel Gläser (@DanielGlaeser, glaeda01@thu.de, ET4/7d)
- **High_Speed_Electronics**: Leon Welsch (@derwelsch, leon.welsch@outlook.com, ET4/7d)
- **Internet_of_Things**: Leon Welsch (@derwelsch, leon.welsch@outlook.com, ET4/7d)
- **Kommunikationssysteme**: Leon Welsch (@derwelsch, leon.welsch@outlook.com, ET4/7d)
- **Leistungselektronik**: Thorben Carl (@ThorbenCarl, thorben.carl@gmail.com, ET7/9d)
- **Wirtschaft**: Thorben Carl (@ThorbenCarl, thorben.carl@gmail.com, ET7/9d)

## Tutoren
- **Mathe 1**: ?
- **Mathe 2**: ? (Florian Hillitzer?, @Dani_13_03?)
- **Mathe 3**: ?
- **ET 1**: ?
- **ET 2**: ?

### Aufgaben der Betreuer und Admins
- Die Bitte an die Admins und Betreuer der entsprechenden Repos, ist diese aktuell zuhalten: Dokumente, Links, etc.
- Jeder, der möchte, kann einen Merge Request erstellen, um Sachen zu ändern. Damit dies nicht im Chaos endet, muss der Betreuer diesen Merge Request kontrollieren, bestätigen und durchführen.
- Wenn der Betreuer sein Studium beendet oder die Hochschule verlässt, so möge er bitte einen neuen Verantwortlichen benennen. 

# :cloud: Historie
Dieser Git-Server ist aus der Cloud von @darulin007 erstanden. <br>
@darulin007 hat eine Vielzahl an Zusammenfassungen für alle Studierende bereitgestellt. Dies möchten wir weiterführen.
Die [Cloud](https://bit.ly/THU_Lernmaterialien) von @darulin007 wird Schritt für Schritt auf diesen Git-Server umziehen.
